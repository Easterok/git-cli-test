import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AnimationDriver, ɵNoopAnimationDriver, ɵWebAnimationsDriver} from '@angular/animations/browser';

const concurrency = window.navigator.hardwareConcurrency;

export function animationFactory(): any {
  return concurrency < 4 ? new ɵNoopAnimationDriver() : new ɵWebAnimationsDriver();
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: AnimationDriver,
      useFactory: animationFactory,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
